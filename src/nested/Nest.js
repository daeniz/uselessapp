import React from 'react'
import { Label } from 'semantic-ui-react';

const Nest = ({ match }) => (
    <div>
      <Label> {match.params.nestId}</Label>
    </div>
  );

  export default Nest