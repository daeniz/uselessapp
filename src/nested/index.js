import React from 'react'
import { Segment, Menu } from 'semantic-ui-react'
import { Link, Route } from 'react-router-dom'
import Nest from './Nest';

const Nested = ({ match }) => {
    return (
        <Segment>
            <Menu inverted color='red'>
                <Menu.Item as={Link} to={`${match.url}/The_Wicked_Nest`}>Wicked</Menu.Item>
                <Menu.Item as={Link} to={`${match.url}/The_Witch_From_The_Nest`}>Witch</Menu.Item>
                <Menu.Item as={Link} to={`${match.url}/The_Bees_Nest`}>Bees</Menu.Item>
                <Menu.Item as={Link} to={`${match.url}/The_Sweet_Nest`}>Sweet</Menu.Item>
            </Menu>
            <div>

                <Route path={`${match.url}/:nestId`} component={Nest} />
                <Route
                    exact
                    path={match.url}
                    render={() => <h3>I dare you to press a button!</h3>}
                />
            </div>


        </Segment>
    )
}

export default Nested