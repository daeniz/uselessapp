import React, { Component } from 'react';
import '../App.css';
import PropTypes from 'prop-types';

const DEFAULT_QUERY = 'redux'
const DEFAULT_HPP = '10'

const PATH_BASE = 'https://hn.algolia.com/api/v1';
const PATH_SEARCH = '/search';
const PARAM_SEARCH = 'query=';
const PARAM_PAGE = 'page='
const PARAM_HPP = 'hitsPerPage=';

class HackerNews extends Component {

  constructor(props) {
    super(props)
    this.state = {
      result: null,
      searchTerm: DEFAULT_QUERY,
    }

    this.setSearchTopStories = this.setSearchTopStories.bind(this);
    this.onDismiss = this.onDismiss.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.fetchSearchTopStories = this.fetchSearchTopStories.bind(this);
  }

  setSearchTopStories(result) {
    //get hits and page from result
    const { hits, page } = result;

    //if page is not zero, let oldhits = hits, else return empty arry
    const oldHits = page !== 0 ? this.state.result.hits : [];

    //merge hits
    const updatedHits = [
      ...oldHits, ...hits
    ]


    this.setState({
      result: { hits: updatedHits, page }
    });
  }



  //synthetic React Event
  onSearchChange(event) {
    this.setState({ searchTerm: event.target.value })
  }


  // onDismiss(id) {
  //   //filter function
  //   const isNotId = item => item.objectID !== id;
  //   //pass function to filter (array function)
  //   const updatedList = this.state.list.filter(isNotId)
  //   //update state
  //   this.setState({ list: updatedList })
  // }

  onDismiss(id) {
    const isNotId = item => item.objectID !== id;
    //pass function to filter (array function)
    const updatedHits = this.state.result.hits.filter(isNotId)
    //update state
    this.setState({
      //result: Object.assign({},this.state.result,{hits: updatedHits}) }) DONT
      result: { ...this.state.result, hits: updatedHits }
    })
  }

  onSearchSubmit(event) {
    const { searchTerm } = this.state
    this.fetchSearchTopStories(searchTerm)
    event.preventDefault();

  }

  fetchSearchTopStories(searchTerm, page = 0) {
    fetch(`${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}&${PARAM_PAGE}${page}&${PARAM_HPP}${DEFAULT_HPP}`)
      .then(response => response.json())
      .then(result => this.setSearchTopStories(result))
      .catch(error => error)
  }

  componentDidMount() {
    const { searchTerm } = this.state;

    this.fetchSearchTopStories(searchTerm)
  }

  render() {

    //Destructuring
    const { searchTerm, result } = this.state
    const page = (result && result.page) || 0;
    if (!result) { return null; }

    return (

      <div className="page">
        <div className="interactions">
          <Search
            value={searchTerm}
            onChange={this.onSearchChange}
            onSubmit={this.onSearchSubmit}>
            Search
          </Search>
        </div>
        {result &&
          <Table
            list={result.hits}
            pattern={searchTerm}
            onDismiss={this.onDismiss}
          />
        }
        <Button onClick={() => this.fetchSearchTopStories(searchTerm, page + 1)}>More</Button>

      </div>
    );
  }
}



const Search = ({ value, onChange, onSubmit, children }) => {

  return (
    <form onSubmit={onSubmit}>{children}

      <input type="text" value={value} onChange={onChange} />
      <button type="submit">
        {children}
      </button>

    </form>
  )
}





const Table = ({ list, onDismiss }) => {

  return (
    <div className="table">
      {list.map(item =>
        <div key={item.objectID} className="table-row">
          <span style={{ width: '40%' }}>
            <a href={item.url}>> {item.title}</a>
          </span>
          <span style={{ width: '30%' }}> {item.author} </span>
          <span style={{ width: '10%' }}> {item.num_comments} </span>
          <span style={{ width: '10%' }}> {item.points} </span>
          <span style={{ width: '10%' }}>
            <Button className="button-inline" onClick={() => onDismiss(item.objectID)}>Dismiss</Button>
          </span>
        </div>
      )}
    </div>
  )


}

const Button = ({ onClick, className = '', children }) => {

  return (
    <button onClick={onClick} className={className} type="button">
      {children}
    </button>
  )
}

Button.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node,
  };

//const isSearched = searchTerm => item => item.title.toLowerCase().includes(searchTerm.toLowerCase());

// const list = [
//   {
//     title: 'React',
//     url: 'https://facebook.github.io/react/',
//     author: 'Jordan Walke',
//     num_comments: 3,
//     points: 4,
//     objectID: 0,
//   },
//   {
//     title: 'Redux',
//     url: 'https://github.com/reactjs/redux',
//     author: 'Dan Abramov, Andrew Clark',
//     num_comments: 2,
//     points: 5,
//     objectID: 1,
//   },
// ];
export default HackerNews;
