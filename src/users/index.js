import React, { Component } from 'react'
import { Segment, Button } from 'semantic-ui-react';
import { observer } from 'mobx-react'

const Countries = observer (class Countries extends Component {

    constructor(props){
        super(props)
        this.addCountry = this.addCountry.bind(this)
    }

    addCountry(){
        this.props.store.addCountry()

        console.log(this.props.store.isLoading)
    }

    render() {
        const { store } = this.props
        return (
            <div>
                <Segment loading={store.isLoading}>
                   
                    {store.countries.map((item,i) => {
                        return (
                            <div key={i}>{item.name}</div>
                        )
                    })}
                     <Button onClick={this.addCountry}>Press to add</Button>
                </Segment>
            </div>
        )
    }

})

export default Countries