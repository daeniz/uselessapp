import React from 'react';
import {Menu} from 'semantic-ui-react'
import{Link} from 'react-router-dom'

const MainMenu = () =>{
    return(
        <Menu stackable attached={true} color='red' inverted>
        <Menu.Item header>Little Useless WebApp</Menu.Item>
        <Menu.Item color='orange' as={Link} to='/'>Main</Menu.Item>
        <Menu.Item as={Link} to='/hackernews'>Hacker News</Menu.Item>
        <Menu.Item as={Link} to='/countries'>Countries</Menu.Item>
        <Menu.Item as={Link} to='/isitfriday'>Is it friday</Menu.Item>
        <Menu.Item as={Link} to='/nested'>Nested Routes</Menu.Item>
        <Menu.Item as={Link} to='/hush'>Hush Hush</Menu.Item>
    </Menu>
    )
}

export default MainMenu