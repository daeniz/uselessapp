import React from 'react'
import {
    Route,
    Redirect,
} from 'react-router-dom'

const pathName = '/'
const loggedIn = false

const PrivateRoute = ({component: Component, ...rest}) =>{
    return(

    <Route {...rest} render={props =>(
        loggedIn ? (
            <Component {...props}/>
        ) : (
            <Redirect to={{
                pathname: pathName,
                state: {from: props.location}
            }}/>
        )
    )}/>
)}



export default PrivateRoute