import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MainMenu from './components/menu';
import { Route } from 'react-router-dom';

//Import views
import HackerNews from './hackernews'
import Main from './main'
import Nested from './nested';
import IsItFriday from './isitfriday';
import Countries from './users';
import {countryStore} from './stores/CountryStore'
import PrivateRoute from './components/privateRoute';
import Hush from './components/hush';


class App extends Component {
  render() {
    return (
      <div>
        <MainMenu />
        <div style={{ padding: '20px' }}>
          <Route exact path="/" component={Main} />
          <Route path="/hackernews" component={HackerNews} />
          <Route path="/countries" render={(props)=><Countries{...props} store={countryStore}/>}/>
          <Route path="/nested" component={Nested} />
          <Route path="/isitfriday"  component={IsItFriday} />
          <PrivateRoute path="/hush" component={Hush}/>
        </div>
      </div>
    );
  }
}

export default App;
