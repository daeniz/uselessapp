import { extendObservable, observable, decorate, action, computed } from 'mobx'

class CountryStore {

    constructor() {
        extendObservable(this, {
            countries: [],
            isLoading: false
        })
        // autorun(()=>this.loadCountries)
        this.loadCountries()
    }

    get numberOfCountries() {
        return this.countries.length;
    }



    loadCountries() {
        this.isLoading = true
        fetch('https://restcountries.eu/rest/v2/all')
            .then(response => response.json())
            .then(result => {
                this.countries = result;
                this.isLoading = false;
            })
            .catch(error => console.log(error))
    }

    
    addCountry() {
        this.countries.push({ name: 'test' })
    }

}
decorate(CountryStore, {
   
    loadCountries: action,
    numberOfCountries: computed,
    removeTenCountries: action,
    addCountry: action
})

export const countryStore = new CountryStore()